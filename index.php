<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity 4</title>
</head>
<body>
    
    <h1>Display Information</h1>

    <h2>Person Information</h2>

    <p>Name: <?= $person->getName(); ?></p>

    <p>Age: <?= $person->getAge(); ?></p>

    <p>Address: <?= $person->getAddress(); ?></p>

    <h2>Student Information</h2>

    <p>Name: <?= $student->getName(); ?></p>

    <p>Age: <?= $student->getAge(); ?></p>

    <p>StudentID: <?= $student->getStudentId(); ?></p>

    <p>Address: <?= $student->getAddress(); ?></p>

    <h2>Employee Information</h2>

    <p>Name: <?= $job->getName(); ?></p>

    <p>Age: <?= $job->getAge(); ?></p>

    <p>Team: <?= $job->getTeam(); ?></p>

    <p>Role: <?= $job->getRole(); ?></p>

    <p>Address: <?= $job->getAddress(); ?></p>

</body>
</html>