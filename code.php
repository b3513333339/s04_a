<?php

class Person {

    protected $name;
    protected $age;
    protected $address;

    public function __construct($name, $age, $address) {
        $this->name = $name;
        $this->age = $age;
        $this->address = $address;
    }

    public function getName() {
        return $this->name;
    }

    public function getAge() {
        return $this->age;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setAge($age) {
        $this->age = $age;
    }

    public function setAddress($address) {
        $this->address = $address;
    }
}

$person = new Person('John Smith', 30, 'Quezon City, Metro Manila');


class Student extends Person {

    protected $studentId;

    public function __construct($name, $age, $studentId, $address) {
        parent::__construct($name, $age, $address);
        $this->studentId = $studentId;  
    }

    public function getStudentId() {
        return $this->studentId;
    }

    public function setStudentId($studentId) {
        $this->studentId = $studentId;
    }
}

$student = new Student('Jane Doe', 20, '2023-1980', 'Makati City, Metro Manila');


class Employee extends Person {

    protected $team;
    protected $role;

    public function __construct($name, $age, $team, $role, $address) {
        parent::__construct($name, $age, $address);
        $this->team = $team;
        $this->role = $role;
    }

    public function getTeam() {
        return $this->team;
    }

    public function getRole() {
        return $this->role;
    }

    public function setTeam($team) {
        $this->team = $team;
    }

    public function setRole($role) {
        $this->role = $role;
    }
}

$job = new Employee('Mark Blain', 35, 'Tech Team', 'Team Lead', 'Pasig City, Metro Manila');

?>